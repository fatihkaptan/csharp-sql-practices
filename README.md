c# ve sql ders notları
---------------------------
BOLUM 1 : .NET Framework Console App Practices with SQL Server 

00_hello world
	> console.write / writeline  methodu ile ekrana yazi yazdir, comnsole.readline methodu ile ekranda entera basana kadar tut
	>

sql_00:
>veri tipleri
	> bit: iki farklı durumu ifade etmek için kulanılır
	> char(10) : sabit uzunluklu veriler için kulanılır ( tc kimlik no gibi)
	> decimal(18,2) : virgülden sonra 2 sayılı decimal sayı
	> nchar(10): latin alfabasi dışındaki veriler için kulanılır.
	> varchar(50) : değişken uzunluklu veriler için kulanılır.
	>>örn : tel, tc char iken isim soyad varchar ile kulanılır. 

>tablo olustururken otomatik id sayaci -> identity spec YES
> options> designers> prevent saving changes > untick yapsan iyi olur
> edit > intellisense > refresh local memory ile tazele hız için

> veritabanı eklerken project> add database > server ve database seç şifreyi barındıran bağlantı kodunu elde et onu koda şu yapıyla ekle 
--------------------------------------------------------------------
	SqlConnection connection = new SqlConnection(@"O KOD"); 
            connection.Open();

            SqlCommand command = new SqlCommand("SQL SORGU KODUN", connection);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read()) //reader ile sırayla belirlenen tabloyu okuma
            {
                Console.WriteLine(reader[0] + " " +  reader[1] + " " + reader[2]);
            }

            connection.Close();
-------------------------------------------------------------------------------
> verilere göre gruplandırma/aynı veri sayısı elde etme:
	select City,Count(*) from TblPerson group by City
	select City,Count(*) from TblPerson group by City order by Count(*) desc 
---------------------------------------------------------------------------------
> ALTER DATABASE SCOPED CONFIGURATION  SET IDENTITY_CACHE = OFF -> id incremnet hafizası sıfırla
---------------------------------------------------------------------------------
>shift + alt + nokta ile aynı variable seç
>SQL de oluşturulan prosedürler programmablity> stored procedures altinda gozukur.

>Visual studioda entity framework vs kullanırken database'i ide arayüzünden project>add new databse üzerinden eklemen gerek.

>entity framework'da ekleme/güncelleme hata kaynağından biri: sql'de oluşturulan tablonun primary_key'i bulunmaması. Bir kolonu primary key olarak seçtikten sonra Visual studioda'da model ortamında sağ tıklayıp "update model from database" yapman gerek.


>

