﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //BUNU EKLİYORUZ
using System.Data;

namespace _08_sql_inner_join
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=KAPTAN_PC\SQLEXPRESS2;Initial Catalog=DbVisit;Persist Security Info=True;User ID=sa;Password=fatih");

            Console.WriteLine("-------------------");

            //INNER JOIN -> data birleştirme
            connection.Open();
            SqlCommand command = new SqlCommand("select FlightID as'Uçuşş Kodu',c1.CountryCapital as'Kalkış Yeri',\r\nc2.CountryCapital as'Varış Yeri',Price as'Fiyat',Capacity as'Kapasite',\r\nFlightDate as'Uçuş Tarih ve Saati' from TblFlight t \r\ninner join TblCountry c1 on t.DepartureLocation=c1.CountryID \r\ninner join TblCountry c2 on t.ArrivalLocation=c2.CountryID", connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read()) { Console.WriteLine(reader[0] + " | " + reader[1] + " | "+ reader[2] + " | " + reader[3] +" | " + reader[4] + " | " + reader[5]); }
            connection.Close();

            Console.WriteLine("-------------------");

            //prosedür olarak database'de tanımladıktan sonra sorguyu kısaca execute edebiliriz.
            //PROCEDURE EXECUTE
            connection.Open();
            SqlCommand command2 = new SqlCommand("exec FlightList", connection);
            SqlDataReader reader2 = command2.ExecuteReader();
            while (reader2.Read()) { Console.WriteLine(reader2[0] + " | " + reader2[1] + " | " + reader2[2] + " | " + reader2[3] + " | " + reader2[4] + " | " + reader2[5]); }
            connection.Close();

            Console.ReadLine();
        }
    }
}
