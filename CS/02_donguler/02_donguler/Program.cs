﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_donguler
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ITERATIONS>>>

            //for->>>>>>
            for (int i = 0; i <= 100; i+=10)
            {
                Console.WriteLine("Counter : " + i);
            }


            //geleneksel faktoriel orn:
            Console.Write("enter number to compute factorial: ");
            int sum = 1;
            int end = Convert.ToInt32(Console.ReadLine());
            for (int i = 1;i <= end;i++) {
                sum = sum * i;
            }
            Console.WriteLine("result : " + sum + "\n\n");


            //while ->>>>>>(Sart saglandigi surece)
            int cnt = 0;
            while (cnt <10) {
                Console.WriteLine("cnt : " + cnt);
                cnt++;           
            }



            Console.ReadLine(); 
        }
    }
}
