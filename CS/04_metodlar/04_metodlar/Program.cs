﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace _04_metodlar
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //void -> no return methods 
            void Message(string name)
            {
                Console.WriteLine("Hello " + name);
                Console.WriteLine("Program Executed.");
                Console.WriteLine("--------------------");
            }

            string[] names = { "Fatih", "Ali", "Veli" };
            foreach (string x in names)
            {
                Message(x);
            }


            //return value methods>>>>
            int topla(int n1 ,int n2)
            {
                return n1 + n2;
            }

            Console.WriteLine(topla(2, 4));

            Console.ReadLine();
        }
    }
}
