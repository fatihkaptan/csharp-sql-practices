﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace _00_Consele_Dersleri
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //00_HELLO_WORLD*******************************
            Console.Write("Hello World! ");
            Console.Write("\nBen Fatih ");

            Console.Write("\n"); //imlec sonunda, \n ile new line yapilir.

            Console.WriteLine("Hello World Again!");//otomatik new line atlar
            Console.WriteLine("Bne Fatih");


            //01_VARIABLES*********************************
            //variable_type variable_name;

            //string variables >>>>>
            string city;
            city = "Istanbul";
            Console.WriteLine(city);

            string capital, country;
            capital = "Ankara";
            country = "Türkiye";
            Console.WriteLine(capital + " " + country);

            //camel case ->  wordSecondword
            string nameSurname;
            nameSurname = "Fatih Kaptanoğlu";

            string job;
            job = "Automation Engineer";
            Console.WriteLine("Name Surname : " + nameSurname + " Your Job: " + job);

            //int variables >>>>
            int n1, n2, sum, divide, multip,extract;
            n1 = 15; n2= 5;
            sum = n1 + n2; divide= n1 / n2; multip= n1 * n2; extract= n1 - n2;
            Console.WriteLine("Addition : " + sum + " Extraction : " + extract);
            Console.WriteLine("Multiplication :" + multip + " Dividing : " + divide);

            //char >>>
            char harf = 'A'; //tek tırnak ile tanımlanır!!!
            Console.WriteLine("char -> " + harf);

            //single-double-decimal>>           
            float nn1 = Convert.ToSingle(23.345 / 2.3451); // 4 byte  -> 32 bit 
            double nn2 = Convert.ToDouble(23.345 / 2.3451); // 8 byte -> 64 bit
            decimal nn3 = Convert.ToDecimal(23.345 / 2.3451); //16 byte
            Console.WriteLine("single : " + nn1);
            Console.WriteLine("double : " + nn2);
            Console.WriteLine("decimal : " + nn3);
            //veritippleri fotilerine bak


            //02_INPUT_FROM_KEYBOARD************************************************
            Console.Write("enter the age: ");
            string age; //girdiler alfanumerik olarak alınır !!! integer alamazsın direk
            age = Console.ReadLine();
            Console.WriteLine("Your age: " + age);

            //Convert Cast>>>>>
            //convert.expectedType(rawData)

            Console.Write("Enter number (0..255): ");
            byte number1; //1 byte -> dec 0..255 (unsigned)
            number1  = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("you enter : " + number1);

            Console.Write("Enter number (+-128): ");
            sbyte number2; //signed
            number2 = Convert.ToSByte(Console.ReadLine());
            Console.WriteLine("you enter : " + number2);

            Console.Write("Enter floating number: ");
            double number3; //signed
            number3 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("you enter : " + number3);
            //DİKKAT !! konsolda virgül ile girilir !!!

            Console.Write("enter character: ");
            char char1 = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("entered char : " + char1);

            Console.ReadLine();

        }
    }
}
