﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //BUNU EKLİYORUZ
using System.Data;

namespace _06_sql_cs_list_delete_update_insert
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=KAPTAN_PC\SQLEXPRESS2;Initial Catalog=DbVisit;Persist Security Info=True;User ID=sa;Password=fatih"); 
            //adres tanımlarken başına @ koy veya / -> // yap 
            
            //listeleme sorgusu
            void listele(){
                connection.Open();
                SqlCommand command = new SqlCommand("select * from TblPerson", connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine(reader[0] + " " + reader[1] + " " + reader[2]);
                }
                connection.Close();
            }

            //ekleme sorgusu
            void ekleme(string visitorName, string visitorCity){            
                connection.Open();
                SqlCommand command = new SqlCommand("insert into TblPerson (Name,City) values (@p1,@p2)", connection);
                command.Parameters.AddWithValue("@p1", visitorName);//command icindeki degiskenleri tanımla
                command.Parameters.AddWithValue("@p2", visitorCity);
                command.ExecuteNonQuery();//yapilan değişikliği veritabanına kaydet
                connection.Close();
            }

            //silme sorgusu
            void silme(int visitorID)
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Delete From TblPerson where Id= @p1", connection);
                command.Parameters.AddWithValue("@p1",visitorID); command.ExecuteNonQuery();
                connection.Close();
            }

            //guncelleme sorgusu
            void sehir_guncelle(int visitorID,string visitorCity) { 
                connection.Open();
                SqlCommand command = new SqlCommand("update  TblPerson Set City=@p2 where Id=@p1", connection);
                command.Parameters.AddWithValue("@p2",visitorCity);
                command.Parameters.AddWithValue("@p1",visitorID);
                command.ExecuteNonQuery();
                connection.Close();            
            }
            
            listele();
            Console.WriteLine("---------------------------------");
            //ekleme("Sabri", "Ankara");
            //silme(13);
            sehir_guncelle(10,"İzmir");
            listele();
            Console.ReadLine();

        }
    }
}
//Data Source=KAPTAN_PC\SQLEXPRESS2;Initial Catalog=DbVisit;Persist Security Info=True;User ID=sa