﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_diziler
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ARRAYS
            string[] cities = { "Adana", "Istanbul", "Ankara", "Izmir" };
            int[] plaque = { 1, 34, 6, 35 };

            for (int i  = 0; i < 4; i++)
            {
                Console.WriteLine(cities[i] + " " + plaque[i]);
            }

            //FOREACH >>>

            foreach (string x in cities)
            {
                Console.WriteLine(x);
            }


            //pratik-> girilen 4 basamakli sayinin basamkalari toplamı bul
            int sum =0;
            Console.Write("Enter 4 digit number: ");
            string inputt = Console.ReadLine();
            foreach (char y in inputt)
            {//dec 48 -> char '0' 
                sum = sum + (Convert.ToInt32(y)-48); 
            }
            Console.WriteLine(sum);

            Console.ReadLine();
        }
    }
}
