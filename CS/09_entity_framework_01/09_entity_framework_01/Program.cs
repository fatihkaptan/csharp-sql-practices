﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using _09_entity_framework_01.Model; //Modeli kullanmak icin bunu ekliyoruz.

namespace _09_entity_framework_01
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // entity framework -> dbfirst 
            //create model folder in solution and add adonet 
            //select efdesigner and select tables you use 

            //tables become class and every column become property 

            //add Model(line6) and create object using entity
            DbVisitEntities db = new DbVisitEntities();

            //LISTING data using entity framework            
            void listele()
            {
                var values = db.TblPerson.ToList(); //var: variable variable type :))
                                                    //because database include several type of variable int,string,datetime...

                //cekilen degerleri foerach ile parcalayıp kullanabiliriz.
                foreach (var items in values)
                {
                    Console.WriteLine(items.Id + " " + items.Name + " " + items.City);
                }
            }
            //listele();
            Console.WriteLine("-----------------------");

            //Adding(INSERT) data using entity framework 
            void adding()
            {
                TblPerson person = new TblPerson();
                person.Name = "Semra Hüsran";
                person.City = "Kütahya";
                db.TblPerson.Add(person);
                db.SaveChanges();
            }        

            listele();
            Console.WriteLine("-----------------------");

            //DELETE data using entity framework
            void delete(int id) //such id=18 
            {
                var value = db.TblPerson.Find(id);
                db.TblPerson.Remove(value);
                db.SaveChanges();
            }

            //delete(19);// person tablosundan 19 id'ye sahip satiri sec ve sil&kaydet
            listele();
            Console.WriteLine("-----------------------");

            //UPDATE data using etity framework
            var value2 =db.TblPerson.Find(17);
            value2.Name = "Muharrem Sel";
            value2.City = "Gaziantep";
            db.SaveChanges();
            listele();
            Console.WriteLine("-----------------------");

            //WHERE find data using entity framework
            var values3 = db.TblPerson.Where(x => x.City =="Manisa").ToList();
            foreach (var item in values3) { Console.WriteLine(item.Name + " " + item.City); }
            //x -> x.Property == "***"  bu landa sorgulama biçimini sorguluyoruz.
            // == yerine diger karsilastırma operatorleri ile sayisal sorgu da yapılaiblir. x>100 tl gibi gibi
            // orn sorgu : x => x.fiyat<50 && x.sehir=="Ankara"  gibi...

            Console.WriteLine("-----------------------");

            //ISTATISTIK 
            var value4 = db.TblPerson.Count();
            Console.WriteLine("Kişi sayısı : " + value4);

            var value5 = db.TblFlight.Average(x => x.Price);
            Console.WriteLine("Ortalama Bilet fiyatı : " + value5);

            //pratik: yolcu sayısı(kapasite) * bilet fiyatı ile toplam gelir hesaplama 
            var value6 = db.TblFlight.ToList();
            double total = 0;
            foreach (var item in value6)
            {
                total = total +  Convert.ToDouble( item.Price) * Convert.ToDouble(item.Capacity);
                //Console.WriteLine(item.Price + " " + item.Capacity);
            }
            Console.WriteLine("Toplam bilet gelirleri: " + total + " usd");

            Console.WriteLine("-----------------------");

            //ALT SORGU
            //örn ülke tablosundaki rusyanın id'sini bulalım, bu id ile uçuş kalkış varış noklarını birlestittirelim
            var value7 = db.TblCountry.Where(x => x.CountryName == "Rusya").Select(y => y.CountryID).FirstOrDefault();
            //Console.WriteLine("id: " + value7);
            var value8 = db.TblFlight.Where(x => x.DepartureLocation == value7).ToList();
            foreach (var item in value8)
            {
                Console.WriteLine("id: " + item.FlightID + " Date: " + item.FlightDate);
            }





            Console.ReadLine();


        }
    }
}
