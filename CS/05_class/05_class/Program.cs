﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_class
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //nesne olusturma:
            // class_name object_name = new class_name();

            Country t = new Country();
            t.Id = 1; 
            t.Name = "Türkiye";
            t.Capital = "Ankara";

            Visitor v= new Visitor();
            v.Id = 1;
            v.Name = "Fatih";
            v.City = "Ankara";
            v.mesaj("Merhaba");
            
            Console.WriteLine(v.Name);

            Console.ReadLine();

        }
    }
}
