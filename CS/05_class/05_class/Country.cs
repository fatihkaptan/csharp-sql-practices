﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_class
{
    class Country
    {
        //properties of class
        public int Id { get; set; }
        public string Name { get; set;}
        public string Capital { get; set;}
    }
}

//not : bu örnekte ülke bir sınıfsa her bir ülke bir nesne olacak,
 //         bu nesnelerin özellikle public veya private olabilir(properties)

