﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_class
{
    class Visitor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }

        public void mesaj(string p) { 
            Console.WriteLine("Message : " + p);
        }
        
    }
}
