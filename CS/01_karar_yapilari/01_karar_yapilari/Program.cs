﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace _01_karar_yapilari
{
    internal class Program
    {
        static void Main(string[] args)
        {

            
            /* operators:
            assign ->       = 
            is eq,lt,bt..-> < > >= <= 
            in not equal->  != 
            and ->          &&
            or ->           ||
            arithmetics ->  +-/*
            mode ->         %
            */

            //00_Decision_Structures>>>>
            //password check example
            string pass = "12345", pass_in, user = "admin", user_in;

            Console.Write("Enter the User Name: ");
            user_in = Console.ReadLine();

            Console.Write("Enter the Password: ");
            pass_in= Console.ReadLine();

            if (pass_in==pass && user==user_in){ Console.WriteLine("Correct");}
            else if (pass_in != pass && user == user_in) { Console.WriteLine("Incorrect Pass"); }
            else if (user_in != user && pass_in == pass) { Console.WriteLine("Incorrect User Id"); }
            else { Console.WriteLine("Incorrect Both"); }


            //switch case ->>  çok şart varsa kullanılır

            Console.Write("Enter English Word: ");
            string word = Console.ReadLine();
            switch (word)
            {
                case "apple": Console.WriteLine("Elma");break;
                case "orange": Console.WriteLine("Portakal"); break;
                case "banana": Console.WriteLine("Muz"); break;
                default: Console.WriteLine("Does Not Exist");break;                    
            }//her case ardından break unutma !!!! 



            Console.ReadLine();
        }
    }
}
