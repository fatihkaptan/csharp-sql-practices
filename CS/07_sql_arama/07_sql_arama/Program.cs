﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //BUNU EKLİYORUZ
using System.Data;


namespace _07_sql_arama
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=KAPTAN_PC\SQLEXPRESS2;Initial Catalog=DbVisit;Persist Security Info=True;User ID=sa;Password=fatih");

            //SEHRE GORE ARAMA
            connection.Open();
            SqlCommand command= new SqlCommand("select * from TblPerson where City=@p1",connection);
            command.Parameters.AddWithValue("@p1", "İzmir");
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read()){ Console.WriteLine(reader[0] + " " + reader[1] + " " + reader[2]); }
            connection.Close();

            Console.WriteLine("-------------------");

            //ISTATISTIK(sum,count,avg....)
            connection.Open();
            SqlCommand command2 = new SqlCommand("select count(*) from TblPerson ", connection);
            SqlDataReader reader2 = command2.ExecuteReader();
            Console.Write("Kisi Sayisi: ");
            while (reader2.Read()) { Console.WriteLine(reader2[0]); }
            connection.Close();

            Console.WriteLine("-------------------");

            //GRUPLANDIRMA (veri icinde mod)
            connection.Open();
            SqlCommand command3 = new SqlCommand("select City,Count(*) from TblPerson group by City order by Count(*) desc", connection);
            SqlDataReader reader3 = command3.ExecuteReader();            
            while (reader3.Read()) { Console.WriteLine(reader3[0] + " " + reader3[1]); }
            connection.Close();



            Console.ReadLine();


        }
    }
}
