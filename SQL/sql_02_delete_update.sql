--3.delete ->  silme islemleri icin
--syntax delete from tablo_Adi  where sartlar

--insert TblPerson(Name,City) values ('Selin Belli','Tokat')
--delete from TblPerson where City='Tokat'

--4.Update -> varolan degeri silemden degistirir.
--syntax: update table_name set yeni_ozellikler where degiscek_data_sartlari


--update TblPerson set City='İstanbul' where City='İzmir'
select * from TblPerson
--update TblPerson set City='Kars' where Id=4 --Genellikle id ile silme-guncelleme yap!
select * from TblPerson