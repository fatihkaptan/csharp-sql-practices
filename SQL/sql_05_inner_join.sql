select FlightID as'Uçuş Kodu',CountryCapital as'Kalkış',
Price as'Fiyat',Capacity as'Kapasite', FlightDate as'Uçuş Tarih ve Saati' 
from TblFlight inner join TblCountry
on TblFlight.DepartureLocation=TblCountry.CountryID 

--aynı işleri kısaltarak kullanmak için son işleçlere harf ata
--bu yöntemle aynı veriyi farklı işleçle yürütüp inner join yapabiliriz
select FlightID as'Uçuş Kodu',c1.CountryCapital as'Kalkış Yeri',
c2.CountryCapital as'Varış Yeri',Price as'Fiyat',Capacity as'Kapasite',
FlightDate as'Uçuş Tarih ve Saati' from TblFlight t 
inner join TblCountry c1 on t.DepartureLocation=c1.CountryID 
inner join TblCountry c2 on t.ArrivalLocation=c2.CountryID

--bu yöntemle oluşturulan sorgular gorundugu gibi uzun olur, 
--bu noktada devreye prosedürler girer, fonskiyon veya methodlar gibi kullanılır...

