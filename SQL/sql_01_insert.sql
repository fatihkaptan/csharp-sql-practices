--2.insert -> deger eklemek icin kullanılır.
--syntax: insert into TABLE(Column1,Column2....) values (Value1,Value2...)

--insert into TblCountry (CountryName,CountryCapital) values ('Rusya','Moskova')
--insert into TblCountry(CountryName) values ('Mısır')
--select * from TblCountry

insert into TblPerson(Id,Name,City) values (7,'Murat Tokat' , 'Tokat')
select * from TblPerson