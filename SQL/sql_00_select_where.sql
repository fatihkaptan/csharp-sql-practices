--DML : Data Manupulation Language - (buyuk-kucuk harf duyarliligi yok)
--1.Select
--2.Insert
--3.Update
--4.Delete

--Select: listeleme için kullanılır.
--syntax: Select * From table_name  ( * -> all demek)
Select * From TblPerson
Select City from TblPerson
Select Name,ID From TblPerson

--şartlı sorgu->  "where"
Select Name from TblPerson where City='İzmir'
Select CountryCapital from TblCountry where CountryName='Fransa'
Select * from TblCountry where CountryID>3
--birden fazla şart için "and" "or" 

select * from TblCountry where CountryID>3 and CountryCapital='Londra'
select * from TblCountry where CountryCapital='Ankara' or CountryCapital='Londra'

